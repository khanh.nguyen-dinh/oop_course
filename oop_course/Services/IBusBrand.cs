﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IBusBrand
    {
        List<Brand> GetList(string sKeyword);
        void Add(Brand brand);
        void Edit(Brand brandOld, Brand brandNew);
        void Delete(Brand brand);
        Brand GetInfo(string brandID);
    }
}
