﻿using Entities;
using Repo;

namespace Services
{
    public class BusBrand : IBusBrand
    {
        private IDalBrand _dalBrand = new DalBrand();
        private const int _idMaxLength = 30;
        private const int _nameMaxLength = 30;

        public List<Brand> GetList(string sKeyword)
        {
            return _dalBrand.GetList(sKeyword);
        }

        public void Add(Brand brand)
        {
            /* Check the validity of the brand */
            if (brand.ID.Length > _idMaxLength || brand.ID.Length == 0)
            {
                throw new Exception("Brand ID length invalid");
            }
            if (brand.Name.Length > _nameMaxLength || brand.Name.Length == 0)
            {
                throw new Exception("Brand Name length invalid");
            }

            _dalBrand.Add(brand);
        }
        public void Edit(Brand brandOld, Brand brandNew)
        {
            /* Check the validity of the old brand */
            if (brandOld.ID.Length > _idMaxLength || brandOld.ID.Length == 0)
            {
                throw new Exception("Brand Old ID length invalid");
            }
            if (brandOld.Name.Length > _nameMaxLength || brandOld.Name.Length == 0)
            {
                throw new Exception("Brand Old Name length invalid");
            }

            /* Check the validity of the new brand */
            if (brandOld.ID.Length > _idMaxLength || brandOld.ID.Length == 0)
            {
                throw new Exception("Brand New ID length invalid");
            }
            if (brandOld.Name.Length > _nameMaxLength || brandOld.Name.Length == 0)
            {
                throw new Exception("Brand New Name length invalid");
            }

            _dalBrand.Edit(brandOld, brandNew);
        }
        public void Delete(Brand brand)
        {
            /* Check the validity of the brand */
            if (brand.ID.Length > _idMaxLength || brand.ID.Length == 0)
            {
                throw new Exception("Brand ID length invalid");
            }
            if (brand.Name.Length > _nameMaxLength || brand.Name.Length == 0)
            {
                throw new Exception("Brand Name length invalid");
            }

            _dalBrand.Delete(brand);
        }
        public Brand GetInfo(string brandID)
        {
            return _dalBrand.GetInfo(brandID);
        }
    }
}
