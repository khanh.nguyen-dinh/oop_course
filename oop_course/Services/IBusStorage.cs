﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IBusStorage
    {
        List<Storage> GetList(string sKeyword, string sFilter);
        void UpdateList(Product productOld, Product productNew);
        void Import(Import import);
        void Export(Export export);
    }
}
