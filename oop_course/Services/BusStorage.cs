﻿using Entities;
using Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class BusStorage : IBusStorage
    {
        private IDalStorage _dalStorage = new DalStorage();

        public List<Storage> GetList(string sKeyword, string sFilter)
        {
            /* Read all storage */
            List<Storage> storageList = _dalStorage.GetList(sKeyword);
            DateOnly dateNow = DateOnly.FromDateTime(DateTime.Now);
            foreach (Storage s in storageList)
            {
                if ("expired" == sFilter)
                {
                    if (0 > dateNow.CompareTo(s.ExpiryDate))
                    {
                        storageList.Remove(s);
                    }
                }
                else if ("unexpired" == sFilter)
                {
                    if (0 < dateNow.CompareTo(s.ExpiryDate))
                    {
                        storageList.Remove(s);
                    }
                }
            }

            ///* In case need filter expired product */
            //if ("expired" == sFilter)
            //{
            //    /* Get expiry list */
            //    List<Storage> expiryStorageList = storageList;
            //    DateOnly dateNow = DateOnly.FromDateTime(DateTime.Now);
            //    foreach (Storage s in expiryStorageList)
            //    {
            //        if (0 > dateNow.CompareTo(s.ExpiryDate))
            //        {
            //            expiryStorageList.Remove(s);
            //        }
            //    }
            //    return expiryStorageList;
            //}

            ///* In case need filter unexpired product */
            //if ("unexpired" == sFilter)
            //{
            //    /* Get unexpiry list */
            //    List<Storage> unexpiryStorageList = storageList;
            //    DateOnly dateNow = DateOnly.FromDateTime(DateTime.Now);
            //    foreach (Storage s in unexpiryStorageList)
            //    {
            //        if (0 <= dateNow.CompareTo(s.ExpiryDate))
            //        {
            //            unexpiryStorageList.Remove(s);
            //        }
            //    }
            //    return unexpiryStorageList;
            //}

            return storageList;
        }
        public void UpdateList(Product productOld, Product productNew)
        {
            _dalStorage.UpdateList(productOld, productNew);
        }
        public void Import(Import import)
        {
            _dalStorage.Import(import);
        }
        public void Export(Export export)
        {
            _dalStorage.Export(export);
        }
    }
}
