﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IBusProduct
    {
        List<Product> GetList(string sKeyword);
        void Add(Product product);
        void Edit(Product productOld, Product productNew);
        void Delete(Product product);
        Product GetInfo(string productID);
    }
}
