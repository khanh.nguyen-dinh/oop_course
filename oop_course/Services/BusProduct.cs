﻿using Entities;
using Repo;

namespace Services
{
    public class BusProduct : IBusProduct
    {
        private IDalProduct _dalProduct = new DalProduct();
        private IDalCategory _dalCategory = new DalCategory();
        private IDalBrand _dalBrand = new DalBrand();

        private const int _idMaxLength = 30;
        private const int _nameMaxLength = 30;

        public List<Product> GetList(string sKeyword)
        {
            return _dalProduct.GetList(sKeyword);
        }

        public void Add(Product product)
        {
            /* Check the validity of the product */
            if (product.ID.Length > _idMaxLength || product.ID.Length == 0)
            {
                throw new Exception("Product ID length invalid");
            }
            if (product.Name.Length > _nameMaxLength || product.Name.Length == 0)
            {
                throw new Exception("Product Name length invalid");
            }

            bool IsExist = false;
            List<Category> categories = _dalCategory.GetList("");
            foreach (Category c in categories) 
            {
                if (product.Category == c.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist) 
            {
                throw new Exception("Product Category does not exist");
            }

            IsExist = false;
            List<Brand> brands = _dalBrand.GetList("");
            foreach (Brand b in brands)
            {
                if (product.Brand == b.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("Product Brand does not exist");
            }
            _dalProduct.Add(product);
        }
        public void Edit(Product productOld, Product productNew)
        {
            /* Check the validity of the old product */
            if (productOld.ID.Length > _idMaxLength || productOld.ID.Length == 0)
            {
                throw new Exception("Product Old ID length invalid");
            }
            if (productOld.Name.Length > _nameMaxLength || productOld.Name.Length == 0)
            {
                throw new Exception("Product Old Name length invalid");
            }

            /* Check the validity of the new product */
            if (productOld.ID.Length > _idMaxLength || productOld.ID.Length == 0)
            {
                throw new Exception("Product New ID length invalid");
            }
            if (productOld.Name.Length > _nameMaxLength || productOld.Name.Length == 0)
            {
                throw new Exception("Product New Name length invalid");
            }

            bool IsExist = false;
            List<Category> categories = _dalCategory.GetList("");
            foreach (Category c in categories)
            {
                if (productNew.Category == c.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("Product Category does not exist");
            }

            IsExist = false;
            List<Brand> brands = _dalBrand.GetList("");
            foreach (Brand b in brands)
            {
                if (productNew.Brand == b.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("Product Brand does not exist");
            }

            _dalProduct.Edit(productOld, productNew);
        }
        public void Delete(Product product)
        {
            /* Check the validity of the product */
            if (product.ID.Length > _idMaxLength || product.ID.Length == 0)
            {
                throw new Exception("Product ID length invalid");
            }
            if (product.Name.Length > _nameMaxLength || product.Name.Length == 0)
            {
                throw new Exception("Product Name length invalid");
            }

            _dalProduct.Delete(product);
        }
        public Product GetInfo(string productID)
        {
            return _dalProduct.GetInfo(productID);
        }
    }
}
