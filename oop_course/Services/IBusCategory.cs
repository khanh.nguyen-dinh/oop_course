﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IBusCategory
    {
        List<Category> GetList(string sKeyword);
        void Add(Category category);
        void Edit(Category categoryOld, Category categoryNew);
        void Delete(Category category);
        Category GetInfo(string categoryID);
    }
}
