﻿using Entities;
using Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class BusCategory : IBusCategory
    {
        private IDalCategory _dalCategory = new DalCategory();
        private const int _idMaxLength = 30;
        private const int _nameMaxLength = 30;

        public List<Category> GetList(string sKeyword)
        {
            return _dalCategory.GetList(sKeyword);
        }

        public void Add(Category category)
        {
            /* Check the validity of the category */
            if (category.ID.Length > _idMaxLength || category.ID.Length == 0)
            {
                throw new Exception("Category ID length invalid");
            }
            if (category.Name.Length > _nameMaxLength || category.Name.Length == 0)
            {
                throw new Exception("Category Name length invalid");
            }

            _dalCategory.Add(category);
        }
        public void Edit(Category categoryOld, Category categoryNew)
        {
            /* Check the validity of the old category */
            if (categoryOld.ID.Length > _idMaxLength || categoryOld.ID.Length == 0)
            {
                throw new Exception("Category Old ID length invalid");
            }
            if (categoryOld.Name.Length > _nameMaxLength || categoryOld.Name.Length == 0)
            {
                throw new Exception("Category Old Name length invalid");
            }

            /* Check the validity of the new category */
            if (categoryOld.ID.Length > _idMaxLength || categoryOld.ID.Length == 0)
            {
                throw new Exception("Category New ID length invalid");
            }
            if (categoryOld.Name.Length > _nameMaxLength || categoryOld.Name.Length == 0)
            {
                throw new Exception("Category New Name length invalid");
            }

            _dalCategory.Edit(categoryOld, categoryNew);
        }
        public void Delete(Category category)
        {
            /* Check the validity of the category */
            if (category.ID.Length > _idMaxLength || category.ID.Length == 0)
            {
                throw new Exception("Category ID length invalid");
            }
            if (category.Name.Length > _nameMaxLength || category.Name.Length == 0)
            {
                throw new Exception("Category Name length invalid");
            }

            _dalCategory.Delete(category);
        }
        public Category GetInfo(string categoryID)
        {
            return _dalCategory.GetInfo(categoryID);
        }
    }
}