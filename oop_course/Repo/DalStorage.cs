﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public class DalStorage : IDalStorage
    {
        private string _filePath = "DataBase/Storage.json";
        public List<Storage> GetList(string sKeyword)
        {
            /* Read the storage list from a file */
            string? sStorage;
            List<Storage> storageList = new List<Storage>();
            StreamReader reader = new StreamReader(_filePath);
            do
            {
                sStorage = reader.ReadLine();
                if (null != sStorage)
                {
                    Storage? storage = JsonConvert.DeserializeObject<Storage>(sStorage);
                    if (null != storage)
                    {
                        storageList.Add(storage);
                    }
                }
            }
            while (null != sStorage);
            reader.Close();

            /* Returns all categories in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return storageList;
            }

            /* Count the number of categories that match the keyword in the storage list */
            List<Storage> searchStorageList = new List<Storage>();
            foreach (Storage s in storageList)
            {
                if (s.ProductName.Contains(sKeyword))
                {
                    searchStorageList.Add(s);
                }
            }
            return searchStorageList;
        }

        public void SaveList(List<Storage> storageList)
        {
            /* Write storage information to file */
            StreamWriter writer = new StreamWriter(_filePath);
            foreach (Storage b in storageList)
            {
                string sData = JsonConvert.SerializeObject(b);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public void UpdateList(Product productOld, Product productNew)
        {
            /* Read all storage list */
            List<Storage> storageList = GetList("");

            /* Replace old products with new products */
            for (int i = 0; i < storageList.Count; i++)
            {
                if (productOld.Name == storageList[i].ProductName)
                {
                    storageList[i].ProductName = productNew.Name;
                    break;
                }
            }

            /* Save new list to file */
            SaveList(storageList);
        }

        public void Import(Import import)
        {

        }
        public void Export(Export export)
        {

        }
    }
}
