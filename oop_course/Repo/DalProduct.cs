﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Repo
{
    public class DalProduct : IDalProduct
    {
        private string _filePath = "DataBase/Product.json";
        public List<Product> GetList(string sKeyword)
        {
            /* Read the product list from a file */
            string? sProduct;
            List<Product> productList = new List<Product>();
            StreamReader reader = new StreamReader(_filePath);
            do
            {
                sProduct = reader.ReadLine();
                if (null != sProduct)
                {
                    Product? product = JsonConvert.DeserializeObject<Product>(sProduct);
                    if (null != product)
                    {
                        productList.Add(product);
                    }
                }
            }
            while (null != sProduct);
            reader.Close();

            /* Returns all products in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return productList;
            }

            /* Count the number of products that match the keyword in the product list */
            List<Product> searchProductList = new List<Product>();
            foreach (Product p in productList) 
            { 
                if (p.ID.Contains(sKeyword) || 
                    p.Name.Contains(sKeyword) ||
                    p.Category.Contains(sKeyword) ||
                    p.Brand.Contains(sKeyword)) 
                {
                    searchProductList.Add(p);
                }
            }
            return searchProductList;
        }

        public void SaveList(List<Product> productList)
        {
            /* Write product information to file */
            StreamWriter writer = new StreamWriter(_filePath);
            foreach (Product p in productList)
            {
                string sData = JsonConvert.SerializeObject(p);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public void Add(Product product)
        {
            /* Get product list */
            List<Product> products = GetList("");
            foreach (Product p in products)
            {
                if (p.ID == product.ID)
                {
                    throw new Exception("The Product ID already exists");
                }
                if (p.Name == product.Name)
                {
                    throw new Exception("The Product Name already exists");
                }
            }

            /* Add new product to the product list */
            products.Insert(0, product);

            /* Save new list */
            SaveList(products);
        }

        public void Edit(Product productOld, Product productNew)
        {
            /* Get product list */
            List<Product> products = GetList("");
            /* The Product must exist */
            bool IsExist = false;
            foreach (Product p in products)
            {
                if (p.ID == productOld.ID)
                {
                    IsExist = true;
                    productOld = p;
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("The Product is not exists");
            }

            /* The new product must not yet exist */
            foreach (Product p in products)
            {
                if (p.ID == productNew.ID && productOld.ID != productNew.ID)
                {
                    throw new Exception("New Product ID already exists");
                }
                if (p.Name == productNew.Name && productOld.Name != productNew.Name)
                {
                    throw new Exception("New Product name already exists");
                }
            }

            /* Update product */
            products.Remove(productOld);
            products.Insert(0, productNew);

            /* Save list */
            SaveList(products);
        }

        public void Delete(Product product)
        {
            /* Get product list */
            List<Product> products = GetList("");
            /* The Product must exist */
            bool IsExist = false;
            foreach (Product p in products) 
            {
                if (p.ID == product.ID || p.Name == product.Name)
                {
                    IsExist = true;
                    products.Remove(p);
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("The Product is not exists");
            }
            /* Save the list */
            SaveList(products);
        }

        public Product GetInfo(string productID)
        {
            /* Get product list */
            List<Product> products = GetList(productID);
            /* Search by Product ID in the list */
            foreach (Product p in products)
            {
                if (productID == p.ID)
                {
                    return p;
                }
            }
            throw new Exception("The Product does not exists");
        }
    }
}
