﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public interface IDalCategory
    {
        List<Category> GetList(string sKeyword);
        void SaveList(List<Category> categoryList);
        void Add(Category category);
        void Edit(Category categoryOld, Category categoryNew);
        void Delete(Category category);
        Category GetInfo(string categoryID);
    }
}
