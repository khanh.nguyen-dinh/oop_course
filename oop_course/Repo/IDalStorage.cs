﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public interface IDalStorage
    {
        List<Storage> GetList(string sKeyword);
        void SaveList(List<Storage> storageList);
        void UpdateList(Product productOld, Product productNew);
        void Import(Import import);
        void Export(Export export);
    }
}
