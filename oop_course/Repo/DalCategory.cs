﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public class DalCategory : IDalCategory
    {
        private string _filePath = "DataBase/Category.json";
        public List<Category> GetList(string sKeyword)
        {
            /* Read the category list from a file */
            string? sCategory;
            List<Category> categoryList = new List<Category>();
            StreamReader reader = new StreamReader(_filePath);
            do
            {
                sCategory = reader.ReadLine();
                if (null != sCategory)
                {
                    Category? category = JsonConvert.DeserializeObject<Category>(sCategory);
                    if (null != category)
                    {
                        categoryList.Add(category);
                    }
                }
            }
            while (null != sCategory);
            reader.Close();

            /* Returns all categories in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return categoryList;
            }

            /* Count the number of categories that match the keyword in the category list */
            List<Category> searchCategoryList = new List<Category>();
            foreach (Category b in categoryList)
            {
                if (b.ID.Contains(sKeyword) || b.Name.Contains(sKeyword))
                {
                    searchCategoryList.Add(b);
                }
            }
            return searchCategoryList;
        }

        public void SaveList(List<Category> categoryList)
        {
            /* Write category information to file */
            StreamWriter writer = new StreamWriter(_filePath);
            foreach (Category b in categoryList)
            {
                string sData = JsonConvert.SerializeObject(b);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public void Add(Category category)
        {
            /* Get category list */
            List<Category> categories = GetList("");
            foreach (Category c in categories)
            {
                if (c.ID == category.ID)
                {
                    throw new Exception("The Category ID already exists");
                }
                if (c.Name == category.Name)
                {
                    throw new Exception("The Category Name already exists");
                }
            }

            /* Add new category to the category list */
            categories.Insert(0, category);

            /* Save new list */
            SaveList(categories);
        }

        public void Edit(Category categoryOld, Category categoryNew)
        {
            /* Get category list */
            List<Category> categories = GetList("");
            /* The Category must exist */
            bool IsExist = false;
            foreach (Category c in categories)
            {
                if (c.ID == categoryOld.ID)
                {
                    IsExist = true;
                    categoryOld = c;
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("The Category is not exists");
            }

            /* The new category must not yet exist */
            foreach (Category c in categories)
            {
                if (c.ID == categoryNew.ID && categoryOld.ID != categoryNew.ID)
                {
                    throw new Exception("New Category ID already exists");
                }
                if (c.Name == categoryNew.Name && categoryOld.Name != categoryNew.Name)
                {
                    throw new Exception("New Category name already exists");
                }
            }

            /* Update category */
            categories.Remove(categoryOld);
            categories.Insert(0, categoryNew);

            /* Save list */
            SaveList(categories);
        }

        public void Delete(Category category)
        {
            /* Get category list */
            List<Category> categories = GetList("");
            /* The Category must exist */
            bool IsExist = false;
            foreach (Category c in categories)
            {
                if (c.ID == category.ID || c.Name == category.Name)
                {
                    IsExist = true;
                    categories.Remove(c);
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("The Category is not exists");
            }
            /* Save the list */
            SaveList(categories);
        }

        public Category GetInfo(string categoryID)
        {
            /* Get category list */
            List<Category> categories = GetList(categoryID);
            /* Search by Category ID in the list */
            foreach (Category c in categories)
            {
                if (categoryID == c.ID)
                {
                    return c;
                }
            }
            throw new Exception("The Category does not exists");
        }
    }
}
