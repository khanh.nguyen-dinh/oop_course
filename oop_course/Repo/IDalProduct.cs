﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public interface IDalProduct
    {
        List<Product> GetList(string sKeyword);
        void SaveList(List<Product> productList);
        void Add(Product product);
        void Edit(Product productOld, Product productNew);
        void Delete(Product product);
        Product GetInfo(string productID);
    }
}
