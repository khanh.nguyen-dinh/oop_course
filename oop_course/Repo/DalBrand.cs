﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Repo
{
    public class DalBrand : IDalBrand
    {
        private string _filePath = "DataBase/Brand.json";
        public List<Brand> GetList(string sKeyword)
        {
            /* Read the brand list from a file */
            string? sBrand;
            List<Brand> brandList = new List<Brand>();
            StreamReader reader = new StreamReader(_filePath);
            do
            {
                sBrand = reader.ReadLine();
                if (null != sBrand)
                {
                    Brand? brand = JsonConvert.DeserializeObject<Brand>(sBrand);
                    if (null != brand)
                    {
                        brandList.Add(brand);
                    }
                }
            }
            while (null != sBrand);
            reader.Close();

            /* Returns all brands in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return brandList;
            }

            /* Count the number of brands that match the keyword in the brand list */
            List<Brand> searchBrandList = new List<Brand>();
            foreach (Brand b in brandList) 
            { 
                if (b.ID.Contains(sKeyword) || b.Name.Contains(sKeyword))
                {
                    searchBrandList.Add(b);
                }
            }
            return searchBrandList;
        }

        public void SaveList(List<Brand> brandList)
        {
            /* Write brand information to file */
            StreamWriter writer = new StreamWriter(_filePath);
            foreach (Brand b in brandList)
            {
                string sData = JsonConvert.SerializeObject(b);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public void Add(Brand brand)
        {
            /* Get brand list */
            List<Brand> brands = GetList("");
            foreach (Brand b in brands)
            {
                if (b.ID == brand.ID)
                {
                    throw new Exception("The Brand ID already exists");
                }
                if (b.Name == brand.Name)
                {
                    throw new Exception("The Brand Name already exists");
                }
            }

            /* Add new brand to the brand list */
            brands.Insert(0, brand);

            /* Save new list */
            SaveList(brands);
        }

        public void Edit(Brand brandOld, Brand brandNew)
        {
            /* Get brand list */
            List<Brand> brands = GetList("");
            /* The Brand must exist */
            bool IsExist = false;
            foreach (Brand b in brands)
            {
                if (b.ID == brandOld.ID)
                {
                    IsExist = true;
                    brandOld = b;
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("The Brand is not exists");
            }

            /* The new brand must not yet exist */
            foreach (Brand b in brands)
            {
                if (b.ID == brandNew.ID && brandOld.ID != brandNew.ID)
                {
                    throw new Exception("New Brand ID already exists");
                }
                if (b.Name == brandNew.Name && brandOld.Name != brandNew.Name)
                {
                    throw new Exception("New Brand name already exists");
                }
            }

            /* Update brand */
            brands.Remove(brandOld);
            brands.Insert(0, brandNew);

            /* Save list */
            SaveList(brands);
        }

        public void Delete(Brand brand)
        {
            /* Get brand list */
            List<Brand> brands = GetList("");
            /* The Brand must exist */
            bool IsExist = false;
            foreach (Brand b in brands) 
            {
                if (b.ID == brand.ID || b.Name == brand.Name)
                {
                    IsExist = true;
                    brands.Remove(b);
                    break;
                }
            }
            if (false == IsExist)
            {
                throw new Exception("The Brand is not exists");
            }
            /* Save the list */
            SaveList(brands);
        }

        public Brand GetInfo(string brandID)
        {
            /* Get brand list */
            List<Brand> brands = GetList(brandID);
            /* Search by Brand ID in the list */
            foreach (Brand b in brands)
            {
                if (brandID == b.ID)
                {
                    return b;
                }
            }
            throw new Exception("The Brand does not exists");
        }
    }
}
