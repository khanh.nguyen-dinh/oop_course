﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages
{
    public class IndexModel : PageModel
    {
        private IBusStorage _busStorage = new BusStorage();
        public List<Storage> storageList { get; set; } = new List<Storage>();

        public SitePage page = new SitePage();
        public string sKeyword { get; set; } = string.Empty;
        public string sFilter { get; set; } = string.Empty;

        public void OnGet()
        {
            /* Try get page index */
            int value = 0;
            if (int.TryParse(Request.Query["page"], out value))
            {
                page.Index = value;
            }

            /* Get search keyword */
            sKeyword = Request.Query["search"];

            /* Get filter selection */
            sFilter = Request.Query["filter"];

            /* Read the Storage list */
            storageList = _busStorage.GetList(sKeyword, sFilter);

            /* Make Storage list page */
            page.Align(storageList.Count);
        }
    }
}
