using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.product
{
    public class AddModel : PageModel
    {
        private IBusProduct _busProduct = new BusProduct(); 
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public string Category { get; set; }
        [BindProperty]
        public string Brand { get; set; }
        [BindProperty]
        public uint Price { get; set; }
        public bool IsAdded { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsAdded = false;
        }
        public void OnPost() 
        {
            IsAdded = true;
            try
            {
                Product product = new Product(Id, Name, Category, Brand, Price);
                _busProduct.Add(product);
                Response.Redirect("/product");
            }
            catch (Exception ex) 
            { 
                StrInfo = ex.Message;
            }
        }
    }
}
