using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.product
{
    public class EditModel : PageModel
    {
        private IBusProduct _busProduct = new BusProduct();
        public Product product;
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public string Category { get; set; }
        [BindProperty]
        public string Brand { get; set; }
        [BindProperty]
        public uint Price { get; set; }
        public bool IsEdited { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsEdited = false;
            try
            {
                product = _busProduct.GetInfo(Request.Query["id"]);
                Id = product.ID;
                Name = product.Name;
                Category = product.Category;
                Brand = product.Brand;
                Price = product.Price;
            }
            catch (Exception ex)
            {
                IsEdited = true;
                StrInfo = ex.Message;
            }
        }
        public void OnPost()
        {
            IsEdited = true;
            try 
            {
                product = _busProduct.GetInfo(Request.Query["id"]);
                Product productNew = new Product(Id, Name, Category, Brand, Price);
                _busProduct.Edit(product, productNew);
                Response.Redirect("/product");
            }
            catch (Exception ex) 
            {
                StrInfo = ex.Message;
            }
        }
    }
}
