﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;
using System.Reflection;

namespace Website.Pages.product
{
    public class IndexModel : PageModel
    {
        private IBusProduct _busProduct = new BusProduct();
        public List<Product> productList { get; set; } = new List<Product>();

        public SitePage page = new SitePage();
        public string sKeyword { get; set; } = string.Empty;
        public void OnGet()
        {
            /* Try get page index */
            int value = 0;
            if (int.TryParse(Request.Query["page"], out value))
            {
                page.Index = value;
            }

            /* Get search keyword */
            sKeyword = Request.Query["search"];

            /* Read the product list */
            productList = _busProduct.GetList(sKeyword);

            /* Make product list page */
            page.Align(productList.Count);
        }
    }
}
