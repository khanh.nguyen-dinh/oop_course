using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.product
{
    public class DeleteModel : PageModel
    {
        private IBusProduct _busProduct = new BusProduct();
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public string Category { get; set; }
        [BindProperty]
        public string Brand { get; set; }
        [BindProperty]
        public uint Price { get; set; }
        public bool IsDeleted { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsDeleted = false;
            Id = Request.Query["id"];
            try
            {
                Product product = _busProduct.GetInfo(Id);
                Id = product.ID;
                Name = product.Name;
                Category = product.Category;
                Brand = product.Brand;
                Price = product.Price;
            }
            catch (Exception ex) 
            {
                IsDeleted = true;
                StrInfo = ex.Message;
            }
        }
        public void OnPost() 
        {
            IsDeleted = true;
            try
            {
                Product product = new Product(Id, Name, Category, Brand, Price);
                _busProduct.Delete(product);
                Response.Redirect("/product");
            }
            catch (Exception ex)
            {
                StrInfo = ex.Message;
            }
        }
    }
}
