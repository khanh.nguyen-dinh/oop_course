using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.brand
{
    public class EditModel : PageModel
    {
        private IBusBrand _busBrand = new BusBrand();
        public Brand brand;
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        public bool IsEdited { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsEdited = false;
            try
            {
                brand = _busBrand.GetInfo(Request.Query["id"]);
                Id = brand.ID;
                Name = brand.Name;
            }
            catch (Exception ex)
            {
                IsEdited = true;
                StrInfo = ex.Message;
            }
        }
        public void OnPost()
        {
            IsEdited = true;
            try 
            {
                brand = _busBrand.GetInfo(Request.Query["id"]);
                Brand brandNew = new Brand(Id, Name);
                _busBrand.Edit(brand, brandNew);
                Response.Redirect("/brand");
            }
            catch (Exception ex) 
            {
                StrInfo = ex.Message;
            }
        }
    }
}
