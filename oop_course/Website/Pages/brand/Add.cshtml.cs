using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.brand
{
    public class AddModel : PageModel
    {
        private IBusBrand _busBrand = new BusBrand(); 
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        public bool IsAdded { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsAdded = false;
        }
        public void OnPost() 
        {
            IsAdded = true;
            try
            {
                Brand brand = new Brand(Id, Name);
                _busBrand.Add(brand);
                Response.Redirect("/brand");
            }
            catch (Exception ex) 
            { 
                StrInfo = ex.Message;
            }
        }
    }
}
