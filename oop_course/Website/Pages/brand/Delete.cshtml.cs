using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.brand
{
    public class DeleteModel : PageModel
    {
        private IBusBrand _busBrand = new BusBrand();
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsDeleted = false;
            Id = Request.Query["id"];
            try
            {
                Brand brand = _busBrand.GetInfo(Id);
                Id = brand.ID;
                Name = brand.Name;
            }
            catch (Exception ex) 
            {
                IsDeleted = true;
                StrInfo = ex.Message;
            }
        }
        public void OnPost() 
        {
            IsDeleted = true;
            try
            {
                Brand brand = new Brand(Id, Name);
                _busBrand.Delete(brand);
                Response.Redirect("/brand");
            }
            catch (Exception ex)
            {
                StrInfo = ex.Message;
            }
        }
    }
}
