﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;
using System.Reflection;

namespace Website.Pages.brand
{
    public class IndexModel : PageModel
    {
        private IBusBrand _busBrand = new BusBrand();
        public List<Brand> BrandList { get; set; } = new List<Brand>();

        public SitePage page = new SitePage();
        public string sKeyword { get; set; } = string.Empty;
        public void OnGet()
        {
            /* Try get page index */
            int value = 0;
            if (int.TryParse(Request.Query["page"], out value))
            {
                page.Index = value;
            }

            /* Get search keyword */
            sKeyword = Request.Query["search"];

            /* Read the brand list */
            BrandList = _busBrand.GetList(sKeyword);

            /* Make brand list page */
            page.Align(BrandList.Count);
        }
    }
}
