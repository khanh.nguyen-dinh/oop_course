using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.category
{
    public class DeleteModel : PageModel
    {
        private IBusCategory _busCategory = new BusCategory();
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsDeleted = false;
            Id = Request.Query["id"];
            try
            {
                Category category = _busCategory.GetInfo(Id);
                Id = category.ID;
                Name = category.Name;
            }
            catch (Exception ex) 
            {
                IsDeleted = true;
                StrInfo = ex.Message;
            }
        }
        public void OnPost() 
        {
            IsDeleted = true;
            try
            {
                Category category = new Category(Id, Name);
                _busCategory.Delete(category);
                Response.Redirect("/category");
            }
            catch (Exception ex)
            {
                StrInfo = ex.Message;
            }
        }
    }
}
