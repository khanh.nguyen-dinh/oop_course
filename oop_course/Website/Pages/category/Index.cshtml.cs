﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;
using System.Reflection;

namespace Website.Pages.category
{
    public class IndexModel : PageModel
    {
        private IBusCategory _busCategory = new BusCategory();
        public List<Category> categoryList { get; set; } = new List<Category>();

        public SitePage page = new SitePage();
        public string sKeyword { get; set; } = string.Empty;
        public void OnGet()
        {
            /* Try get page index */
            int value = 0;
            if (int.TryParse(Request.Query["page"], out value))
            {
                page.Index = value;
            }

            /* Get search keyword */
            sKeyword = Request.Query["search"];

            /* Read the category list */
            categoryList = _busCategory.GetList(sKeyword);

            /* Make category list page */
            page.Align(categoryList.Count);
        }
    }
}
