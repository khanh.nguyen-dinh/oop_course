using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.category
{
    public class EditModel : PageModel
    {
        private IBusCategory _busCategory = new BusCategory();
        public Category category;
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        public bool IsEdited { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsEdited = false;
            try
            {
                category = _busCategory.GetInfo(Request.Query["id"]);
                Id = category.ID;
                Name = category.Name;
            }
            catch (Exception ex)
            {
                IsEdited = true;
                StrInfo = ex.Message;
            }
        }
        public void OnPost()
        {
            IsEdited = true;
            try 
            {
                category = _busCategory.GetInfo(Request.Query["id"]);
                Category categoryNew = new Category(Id, Name);
                _busCategory.Edit(category, categoryNew);
                Response.Redirect("/category");
            }
            catch (Exception ex) 
            {
                StrInfo = ex.Message;
            }
        }
    }
}
