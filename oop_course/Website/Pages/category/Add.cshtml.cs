using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Services;

namespace Website.Pages.category
{
    public class AddModel : PageModel
    {
        private IBusCategory _busCategory = new BusCategory(); 
        [BindProperty]
        public string Id { get; set; }
        [BindProperty]
        public string Name { get; set; }
        public bool IsAdded { get; set; }
        public string StrInfo { get; set; }
        public void OnGet()
        {
            IsAdded = false;
        }
        public void OnPost() 
        {
            IsAdded = true;
            try
            {
                Category category = new Category(Id, Name);
                _busCategory.Add(category);
                Response.Redirect("/category");
            }
            catch (Exception ex) 
            { 
                StrInfo = ex.Message;
            }
        }
    }
}
