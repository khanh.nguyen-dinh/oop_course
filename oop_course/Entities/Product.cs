﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Product
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public uint Price { get; set; }
        public Product(string id, string name, string category, string brand, uint price)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("Product ID invalid");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Product Name invalid");
            }
            if (string.IsNullOrEmpty(category))
            {
                throw new Exception("Product Category invalid");
            }
            if (string.IsNullOrEmpty(brand))
            {
                throw new Exception("Product Brand invalid");
            }
            if (price < 0)
            {
                throw new Exception("Product Price invalid");
            }
            ID = id;
            Name = name;
            Category = category;
            Brand = brand;
            Price = price;
        }
    }
}
