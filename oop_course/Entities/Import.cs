﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Import
    {
        public string ID { get; set; }
        public string User { get; set; }
        public DateOnly Date { get; set; }
        public List<Storage> Storages { get; set; }
    }
}
