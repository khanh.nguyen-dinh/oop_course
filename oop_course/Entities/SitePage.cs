﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class SitePage
    {
        public int ItemPerPage { get; set; }
        public int Index {  get; set; }
        public int Total { get; set; }
        public int ItemStart { get; set; }
        public int ItemEnd { get; set; }
        public int ItemTotal { get; set; }
        public SitePage() 
        {
            /* Default value */
            ItemPerPage = 10;
            Index = 1;
            Total = 1;
            ItemStart = 1;
            ItemEnd = 1;
            ItemTotal = 1;
        }

        public void Align (int size)
        {
            if (size > 0)
            {
                /* Calculate the number of pages based on the total number of item in the list */
                Total = (size - 1) / ItemPerPage + 1;

                /* Check page index exceptions */
                if (Index == 0 || Index > Total)
                {
                    Index = 1;
                }

                /* Calculate the Index of the item that will be displayed */
                ItemStart = (Index - 1) * ItemPerPage + 1;
                ItemEnd = ItemStart + ItemPerPage - 1;
                ItemTotal = size;
                if (Index == Total)
                {
                    ItemEnd = ItemStart + (size - 1) % ItemPerPage;
                }
            }
        }
    }
}
