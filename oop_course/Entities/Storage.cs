﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Storage
    {
        public string ProductName { get; set; }
        public uint Quantity { get; set; }
        public DateOnly ManufactureDate { get; set; }
        public DateOnly ExpiryDate { get; set; }
    }
}
