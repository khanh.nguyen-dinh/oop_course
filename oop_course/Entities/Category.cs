﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Category
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public Category(string id, string name)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("Category ID invalid");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Category Name invalid");
            }
            ID = id;
            Name = name;
        }
    }
}
