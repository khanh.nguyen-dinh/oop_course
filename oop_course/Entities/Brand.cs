﻿namespace Entities
{
    public class Brand
    {
        public string ID { get; set; } 
        public string Name { get; set; }

        public Brand (string id, string name)
        {
            if (string.IsNullOrEmpty(id)) 
            {
                throw new Exception("Brand ID invalid");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Brand Name invalid");
            }
            ID = id;
            Name = name;
        }
    }
}
